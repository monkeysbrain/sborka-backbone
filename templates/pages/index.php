<?
/**/
/** ВНИМАНИЕ!
* Этот файл сгенерирован автоматически и не подлежит редактированию.
* Исходники лежат в папке templates/src/jade
*/
/**/
?><?
define('PAGE_TITLE','Главная страница');
define('PAGE_SECTION_ID','0');
define('PAGE_JS_VIEW','Index');
?>
<div data-view="<?=PAGE_JS_VIEW?>" class="page page--index">
  <div class="container">
    <!-- Тестовый контент-->
    <div data-wait-to-load="data-wait-to-load" style="background-image:url(&quot;templates/src/images/pixel.gif&quot;)" class="example-load-img"></div><img data-wait-to-load="templates/src/images/pixel.gif" src="" class="example-load-img"/>
    <!-- Symbol SVG-->
    <svg role="presentation" class="svg icon_svg--test-star-fullsize">
      <title>
      </title>
      <use xlink:href="<?=SITE_URL?>templates/src/images/symbols.svg?v=<?=SVG_SPRITE_VERSION?>#test-star"></use>
    </svg>
    <!-- Генерируемые классы для спрайтов-->
    <div class="icon_svg--test-test"></div>
    <div class="icon_png--test-test"></div>
    <!-- Вставка спрайтов через миксин, см. templates/src/styl/modules/test.styl-->
    <div class="test__icon-svg"></div>
    <div class="test__icon-png"></div>
  </div>
</div>