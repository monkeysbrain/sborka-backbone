<?
/**/
/** ВНИМАНИЕ!
* Этот файл сгенерирован автоматически и не подлежит редактированию.
* Исходники лежат в папке templates/src/jade
*/
/**/
?><?
$sourceSuffix = '';
?><!DOCTYPE html>
<html>
  <head>
        <meta charset="utf-8">
        <base href="<?=SITE_URL?>">
        <title><?=PAGE_TITLE?></title>
        <meta content="Описание страницы" name="description">
        <meta content="Ключевые слова" name="keywords">
        <meta property="og:type" content="website">
        <link rel="image_src">
        <meta name="author">
        <meta content="telephone=no" name="format-detection">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="HandheldFriendly" content="true">
        <meta name="MobileOptimized" content="320">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta property="og:title" content="Заголовок для шеринга">
        <meta property="og:image" content="">
        <meta property="og:site_name" content="Название сайта для шеринга">
        <meta property="og:description" content="Описание для шеринга">
        <meta name="twitter:title" content="Заголовок для шеринга">
        <meta name="twitter:image:src" content="">
        <meta name="twitter:description" content="Описание для шеринга">
        <link href="<?=urlV('templates/build/css/styles'.$sourceSuffix.'.css')?>" rel="stylesheet" type="text/css">
  </head>
  <body>
    <script src="<?=urlV("templates/build/js/init".$sourceSuffix.".js")?>"></script>
        <header class="header">
          <div class="container">
            <nav><a href="./">Home</a><a href="page1?asd=asd">Page1</a></nav>
          </div>
        </header>
    <main class="grid"><?=$CONTENT?></main>
    <div class="loader js-loader">
      <div class="loader_box"></div>
    </div>
        <footer class="footer">
          <div class="container">&copy; Футер</div>
        </footer>
    <script src="<?=urlV("templates/build/js/libs".$sourceSuffix.".js")?>"></script>
    <script src="<?=urlV("templates/build/js/scripts".$sourceSuffix.".js")?>"></script>
  </body>
</html>