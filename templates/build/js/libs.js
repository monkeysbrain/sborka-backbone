!function(root, factory) {
	"function" == typeof define && define.amd ? // AMD. Register as an anonymous module unless amdModuleId is set
		define([], function() {
			return root.svg4everybody = factory();
		}) : "object" == typeof exports ? module.exports = factory() : root.svg4everybody = factory();
}(this, function() {
	/*! svg4everybody v2.1.0 | github.com/jonathantneal/svg4everybody */
	function embed(svg, target) {
		// if the target exists
		if (target) {
			// create a document fragment to hold the contents of the target
			var fragment = document.createDocumentFragment(), viewBox = !svg.getAttribute("viewBox") && target.getAttribute("viewBox");
			// conditionally set the viewBox on the svg
			viewBox && svg.setAttribute("viewBox", viewBox);
			// copy the contents of the clone into the fragment
			for (// clone the target
				var clone = target.cloneNode(!0); clone.childNodes.length; ) {
				fragment.appendChild(clone.firstChild);
			}
			// append the fragment into the svg
			svg.appendChild(fragment);
		}
	}
	function loadreadystatechange(xhr) {
		// listen to changes in the request
		xhr.onreadystatechange = function() {
			// if the request is ready
			if (4 === xhr.readyState) {
				// get the cached html document
				var cachedDocument = xhr._cachedDocument;
				// ensure the cached html document based on the xhr response
				cachedDocument || (cachedDocument = xhr._cachedDocument = document.implementation.createHTMLDocument(""),
					cachedDocument.body.innerHTML = xhr.responseText, xhr._cachedTarget = {}), // clear the xhr embeds list and embed each item
					xhr._embeds.splice(0).map(function(item) {
						// get the cached target
						var target = xhr._cachedTarget[item.id];
						// ensure the cached target
						target || (target = xhr._cachedTarget[item.id] = cachedDocument.getElementById(item.id)),
							// embed the target into the svg
							embed(item.svg, target);
					});
			}
		}, // test the ready state change immediately
			xhr.onreadystatechange();
	}
	function svg4everybody(rawopts) {
		function oninterval() {
			// while the index exists in the live <use> collection
			for (// get the cached <use> index
				var index = 0; index < uses.length; ) {
				// get the current <use>
				var use = uses[index], svg = use.parentNode;
				if (svg && /svg/i.test(svg.nodeName)) {
					var src = use.getAttribute("xlink:href");
					if (polyfill && (!opts.validate || opts.validate(src, svg, use))) {
						// remove the <use> element
						svg.removeChild(use);
						// parse the src and get the url and id
						var srcSplit = src.split("#"), url = srcSplit.shift(), id = srcSplit.join("#");
						// if the link is external
						if (url.length) {
							// get the cached xhr request
							var xhr = requests[url];
							// ensure the xhr request exists
							xhr || (xhr = requests[url] = new XMLHttpRequest(), xhr.open("GET", url), xhr.send(),
								xhr._embeds = []), // add the svg and id as an item to the xhr embeds list
								xhr._embeds.push({
									svg: svg,
									id: id
								}), // prepare the xhr ready state change event
								loadreadystatechange(xhr);
						} else {
							// embed the local id into the svg
							embed(svg, document.getElementById(id));
						}
					}
				} else {
					// increase the index when the previous value was not "valid"
					++index;
				}
			}
			// continue the interval
			requestAnimationFrame(oninterval, 67);
		}
		var polyfill, opts = Object(rawopts), newerIEUA = /\bTrident\/[567]\b|\bMSIE (?:9|10)\.0\b/, webkitUA = /\bAppleWebKit\/(\d+)\b/, olderEdgeUA = /\bEdge\/12\.(\d+)\b/;
		polyfill = "polyfill" in opts ? opts.polyfill : newerIEUA.test(navigator.userAgent) || (navigator.userAgent.match(olderEdgeUA) || [])[1] < 10547 || (navigator.userAgent.match(webkitUA) || [])[1] < 537;
		// create xhr requests object
		var requests = {}, requestAnimationFrame = window.requestAnimationFrame || setTimeout, uses = document.getElementsByTagName("use");
		// conditionally start the interval if the polyfill is active
		polyfill && oninterval();
	}
	return svg4everybody;
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL2d1bHAvbW9kdWxlcy9zdmctc3ByaXRlL2Fzc2V0cy9qcy9saWJzL3N2ZzRldmVyeWJvZHkuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImxpYnMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIhZnVuY3Rpb24ocm9vdCwgZmFjdG9yeSkge1xyXG5cdFwiZnVuY3Rpb25cIiA9PSB0eXBlb2YgZGVmaW5lICYmIGRlZmluZS5hbWQgPyAvLyBBTUQuIFJlZ2lzdGVyIGFzIGFuIGFub255bW91cyBtb2R1bGUgdW5sZXNzIGFtZE1vZHVsZUlkIGlzIHNldFxyXG5cdFx0ZGVmaW5lKFtdLCBmdW5jdGlvbigpIHtcclxuXHRcdFx0cmV0dXJuIHJvb3Quc3ZnNGV2ZXJ5Ym9keSA9IGZhY3RvcnkoKTtcclxuXHRcdH0pIDogXCJvYmplY3RcIiA9PSB0eXBlb2YgZXhwb3J0cyA/IG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeSgpIDogcm9vdC5zdmc0ZXZlcnlib2R5ID0gZmFjdG9yeSgpO1xyXG59KHRoaXMsIGZ1bmN0aW9uKCkge1xyXG5cdC8qISBzdmc0ZXZlcnlib2R5IHYyLjEuMCB8IGdpdGh1Yi5jb20vam9uYXRoYW50bmVhbC9zdmc0ZXZlcnlib2R5ICovXHJcblx0ZnVuY3Rpb24gZW1iZWQoc3ZnLCB0YXJnZXQpIHtcclxuXHRcdC8vIGlmIHRoZSB0YXJnZXQgZXhpc3RzXHJcblx0XHRpZiAodGFyZ2V0KSB7XHJcblx0XHRcdC8vIGNyZWF0ZSBhIGRvY3VtZW50IGZyYWdtZW50IHRvIGhvbGQgdGhlIGNvbnRlbnRzIG9mIHRoZSB0YXJnZXRcclxuXHRcdFx0dmFyIGZyYWdtZW50ID0gZG9jdW1lbnQuY3JlYXRlRG9jdW1lbnRGcmFnbWVudCgpLCB2aWV3Qm94ID0gIXN2Zy5nZXRBdHRyaWJ1dGUoXCJ2aWV3Qm94XCIpICYmIHRhcmdldC5nZXRBdHRyaWJ1dGUoXCJ2aWV3Qm94XCIpO1xyXG5cdFx0XHQvLyBjb25kaXRpb25hbGx5IHNldCB0aGUgdmlld0JveCBvbiB0aGUgc3ZnXHJcblx0XHRcdHZpZXdCb3ggJiYgc3ZnLnNldEF0dHJpYnV0ZShcInZpZXdCb3hcIiwgdmlld0JveCk7XHJcblx0XHRcdC8vIGNvcHkgdGhlIGNvbnRlbnRzIG9mIHRoZSBjbG9uZSBpbnRvIHRoZSBmcmFnbWVudFxyXG5cdFx0XHRmb3IgKC8vIGNsb25lIHRoZSB0YXJnZXRcclxuXHRcdFx0XHR2YXIgY2xvbmUgPSB0YXJnZXQuY2xvbmVOb2RlKCEwKTsgY2xvbmUuY2hpbGROb2Rlcy5sZW5ndGg7ICkge1xyXG5cdFx0XHRcdGZyYWdtZW50LmFwcGVuZENoaWxkKGNsb25lLmZpcnN0Q2hpbGQpO1xyXG5cdFx0XHR9XHJcblx0XHRcdC8vIGFwcGVuZCB0aGUgZnJhZ21lbnQgaW50byB0aGUgc3ZnXHJcblx0XHRcdHN2Zy5hcHBlbmRDaGlsZChmcmFnbWVudCk7XHJcblx0XHR9XHJcblx0fVxyXG5cdGZ1bmN0aW9uIGxvYWRyZWFkeXN0YXRlY2hhbmdlKHhocikge1xyXG5cdFx0Ly8gbGlzdGVuIHRvIGNoYW5nZXMgaW4gdGhlIHJlcXVlc3RcclxuXHRcdHhoci5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbigpIHtcclxuXHRcdFx0Ly8gaWYgdGhlIHJlcXVlc3QgaXMgcmVhZHlcclxuXHRcdFx0aWYgKDQgPT09IHhoci5yZWFkeVN0YXRlKSB7XHJcblx0XHRcdFx0Ly8gZ2V0IHRoZSBjYWNoZWQgaHRtbCBkb2N1bWVudFxyXG5cdFx0XHRcdHZhciBjYWNoZWREb2N1bWVudCA9IHhoci5fY2FjaGVkRG9jdW1lbnQ7XHJcblx0XHRcdFx0Ly8gZW5zdXJlIHRoZSBjYWNoZWQgaHRtbCBkb2N1bWVudCBiYXNlZCBvbiB0aGUgeGhyIHJlc3BvbnNlXHJcblx0XHRcdFx0Y2FjaGVkRG9jdW1lbnQgfHwgKGNhY2hlZERvY3VtZW50ID0geGhyLl9jYWNoZWREb2N1bWVudCA9IGRvY3VtZW50LmltcGxlbWVudGF0aW9uLmNyZWF0ZUhUTUxEb2N1bWVudChcIlwiKSxcclxuXHRcdFx0XHRcdGNhY2hlZERvY3VtZW50LmJvZHkuaW5uZXJIVE1MID0geGhyLnJlc3BvbnNlVGV4dCwgeGhyLl9jYWNoZWRUYXJnZXQgPSB7fSksIC8vIGNsZWFyIHRoZSB4aHIgZW1iZWRzIGxpc3QgYW5kIGVtYmVkIGVhY2ggaXRlbVxyXG5cdFx0XHRcdFx0eGhyLl9lbWJlZHMuc3BsaWNlKDApLm1hcChmdW5jdGlvbihpdGVtKSB7XHJcblx0XHRcdFx0XHRcdC8vIGdldCB0aGUgY2FjaGVkIHRhcmdldFxyXG5cdFx0XHRcdFx0XHR2YXIgdGFyZ2V0ID0geGhyLl9jYWNoZWRUYXJnZXRbaXRlbS5pZF07XHJcblx0XHRcdFx0XHRcdC8vIGVuc3VyZSB0aGUgY2FjaGVkIHRhcmdldFxyXG5cdFx0XHRcdFx0XHR0YXJnZXQgfHwgKHRhcmdldCA9IHhoci5fY2FjaGVkVGFyZ2V0W2l0ZW0uaWRdID0gY2FjaGVkRG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoaXRlbS5pZCkpLFxyXG5cdFx0XHRcdFx0XHRcdC8vIGVtYmVkIHRoZSB0YXJnZXQgaW50byB0aGUgc3ZnXHJcblx0XHRcdFx0XHRcdFx0ZW1iZWQoaXRlbS5zdmcsIHRhcmdldCk7XHJcblx0XHRcdFx0XHR9KTtcclxuXHRcdFx0fVxyXG5cdFx0fSwgLy8gdGVzdCB0aGUgcmVhZHkgc3RhdGUgY2hhbmdlIGltbWVkaWF0ZWx5XHJcblx0XHRcdHhoci5vbnJlYWR5c3RhdGVjaGFuZ2UoKTtcclxuXHR9XHJcblx0ZnVuY3Rpb24gc3ZnNGV2ZXJ5Ym9keShyYXdvcHRzKSB7XHJcblx0XHRmdW5jdGlvbiBvbmludGVydmFsKCkge1xyXG5cdFx0XHQvLyB3aGlsZSB0aGUgaW5kZXggZXhpc3RzIGluIHRoZSBsaXZlIDx1c2U+IGNvbGxlY3Rpb25cclxuXHRcdFx0Zm9yICgvLyBnZXQgdGhlIGNhY2hlZCA8dXNlPiBpbmRleFxyXG5cdFx0XHRcdHZhciBpbmRleCA9IDA7IGluZGV4IDwgdXNlcy5sZW5ndGg7ICkge1xyXG5cdFx0XHRcdC8vIGdldCB0aGUgY3VycmVudCA8dXNlPlxyXG5cdFx0XHRcdHZhciB1c2UgPSB1c2VzW2luZGV4XSwgc3ZnID0gdXNlLnBhcmVudE5vZGU7XHJcblx0XHRcdFx0aWYgKHN2ZyAmJiAvc3ZnL2kudGVzdChzdmcubm9kZU5hbWUpKSB7XHJcblx0XHRcdFx0XHR2YXIgc3JjID0gdXNlLmdldEF0dHJpYnV0ZShcInhsaW5rOmhyZWZcIik7XHJcblx0XHRcdFx0XHRpZiAocG9seWZpbGwgJiYgKCFvcHRzLnZhbGlkYXRlIHx8IG9wdHMudmFsaWRhdGUoc3JjLCBzdmcsIHVzZSkpKSB7XHJcblx0XHRcdFx0XHRcdC8vIHJlbW92ZSB0aGUgPHVzZT4gZWxlbWVudFxyXG5cdFx0XHRcdFx0XHRzdmcucmVtb3ZlQ2hpbGQodXNlKTtcclxuXHRcdFx0XHRcdFx0Ly8gcGFyc2UgdGhlIHNyYyBhbmQgZ2V0IHRoZSB1cmwgYW5kIGlkXHJcblx0XHRcdFx0XHRcdHZhciBzcmNTcGxpdCA9IHNyYy5zcGxpdChcIiNcIiksIHVybCA9IHNyY1NwbGl0LnNoaWZ0KCksIGlkID0gc3JjU3BsaXQuam9pbihcIiNcIik7XHJcblx0XHRcdFx0XHRcdC8vIGlmIHRoZSBsaW5rIGlzIGV4dGVybmFsXHJcblx0XHRcdFx0XHRcdGlmICh1cmwubGVuZ3RoKSB7XHJcblx0XHRcdFx0XHRcdFx0Ly8gZ2V0IHRoZSBjYWNoZWQgeGhyIHJlcXVlc3RcclxuXHRcdFx0XHRcdFx0XHR2YXIgeGhyID0gcmVxdWVzdHNbdXJsXTtcclxuXHRcdFx0XHRcdFx0XHQvLyBlbnN1cmUgdGhlIHhociByZXF1ZXN0IGV4aXN0c1xyXG5cdFx0XHRcdFx0XHRcdHhociB8fCAoeGhyID0gcmVxdWVzdHNbdXJsXSA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpLCB4aHIub3BlbihcIkdFVFwiLCB1cmwpLCB4aHIuc2VuZCgpLFxyXG5cdFx0XHRcdFx0XHRcdFx0eGhyLl9lbWJlZHMgPSBbXSksIC8vIGFkZCB0aGUgc3ZnIGFuZCBpZCBhcyBhbiBpdGVtIHRvIHRoZSB4aHIgZW1iZWRzIGxpc3RcclxuXHRcdFx0XHRcdFx0XHRcdHhoci5fZW1iZWRzLnB1c2goe1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRzdmc6IHN2ZyxcclxuXHRcdFx0XHRcdFx0XHRcdFx0aWQ6IGlkXHJcblx0XHRcdFx0XHRcdFx0XHR9KSwgLy8gcHJlcGFyZSB0aGUgeGhyIHJlYWR5IHN0YXRlIGNoYW5nZSBldmVudFxyXG5cdFx0XHRcdFx0XHRcdFx0bG9hZHJlYWR5c3RhdGVjaGFuZ2UoeGhyKTtcclxuXHRcdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0XHQvLyBlbWJlZCB0aGUgbG9jYWwgaWQgaW50byB0aGUgc3ZnXHJcblx0XHRcdFx0XHRcdFx0ZW1iZWQoc3ZnLCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChpZCkpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdC8vIGluY3JlYXNlIHRoZSBpbmRleCB3aGVuIHRoZSBwcmV2aW91cyB2YWx1ZSB3YXMgbm90IFwidmFsaWRcIlxyXG5cdFx0XHRcdFx0KytpbmRleDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdFx0Ly8gY29udGludWUgdGhlIGludGVydmFsXHJcblx0XHRcdHJlcXVlc3RBbmltYXRpb25GcmFtZShvbmludGVydmFsLCA2Nyk7XHJcblx0XHR9XHJcblx0XHR2YXIgcG9seWZpbGwsIG9wdHMgPSBPYmplY3QocmF3b3B0cyksIG5ld2VySUVVQSA9IC9cXGJUcmlkZW50XFwvWzU2N11cXGJ8XFxiTVNJRSAoPzo5fDEwKVxcLjBcXGIvLCB3ZWJraXRVQSA9IC9cXGJBcHBsZVdlYktpdFxcLyhcXGQrKVxcYi8sIG9sZGVyRWRnZVVBID0gL1xcYkVkZ2VcXC8xMlxcLihcXGQrKVxcYi87XHJcblx0XHRwb2x5ZmlsbCA9IFwicG9seWZpbGxcIiBpbiBvcHRzID8gb3B0cy5wb2x5ZmlsbCA6IG5ld2VySUVVQS50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQpIHx8IChuYXZpZ2F0b3IudXNlckFnZW50Lm1hdGNoKG9sZGVyRWRnZVVBKSB8fCBbXSlbMV0gPCAxMDU0NyB8fCAobmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCh3ZWJraXRVQSkgfHwgW10pWzFdIDwgNTM3O1xyXG5cdFx0Ly8gY3JlYXRlIHhociByZXF1ZXN0cyBvYmplY3RcclxuXHRcdHZhciByZXF1ZXN0cyA9IHt9LCByZXF1ZXN0QW5pbWF0aW9uRnJhbWUgPSB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8IHNldFRpbWVvdXQsIHVzZXMgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcInVzZVwiKTtcclxuXHRcdC8vIGNvbmRpdGlvbmFsbHkgc3RhcnQgdGhlIGludGVydmFsIGlmIHRoZSBwb2x5ZmlsbCBpcyBhY3RpdmVcclxuXHRcdHBvbHlmaWxsICYmIG9uaW50ZXJ2YWwoKTtcclxuXHR9XHJcblx0cmV0dXJuIHN2ZzRldmVyeWJvZHk7XHJcbn0pOyJdfQ==
